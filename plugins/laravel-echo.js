import Echo from "laravel-echo"

export default () => {
    window.io = require('socket.io-client');

    window.Echo = new Echo({
        broadcaster: 'socket.io',
        host: 'https://srv.rvkolosov.ru:8443',
        disableStats: true,
        encrypted: true,
        authEndpoint: 'https://srv.rvkolosov.ru/broadcasting/auth',
        auth: {
            headers: {
                Authorization: window.localStorage.getItem('auth._token.password_grant')
            },
        },
    });
}